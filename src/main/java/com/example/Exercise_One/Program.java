package com.example.Exercise_One;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;


public class Program {

    public static int birthdayCakeCandles(int n, int[] candles)
    {
        //get max height of candles
        int max = candles[0];
        for(int i = 0; i < n; i++) {
            if (max < candles[i])
                max = candles[i];
        }
        //get count of tallest candles
        int countOfTallestCandles = 0;
        for(int i = 0; i < n; i++) {
            if (max == candles[i])
                countOfTallestCandles++;
        }

        return countOfTallestCandles;
    }

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(reader.readLine());
        int[] candles = Arrays.stream(reader.readLine().split("\\s")).mapToInt(Integer::parseInt).toArray();
        System.out.println(birthdayCakeCandles(n, candles));

    }

}
